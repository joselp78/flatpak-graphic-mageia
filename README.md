# MageiaFlat

MageiaFlat is a small development, to provide a graphical interface in Mageia for the installation of Flathub store applications.

Although there are other applications such as Discover that support this feature, MageiaFlat requires minimal installation and only takes care of making it easy for the user to find and install these applications.

